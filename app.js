/* eslint-disable no-undef */
const express = require('express');
const cors = require('cors');

// eslint-disable-next-line no-unused-vars
const colors = require('colors');
const bodyParser = require('body-parser');
// eslint-disable-next-line no-unused-vars
const dotenv = require('dotenv').config();

const errorHandler = require('./middleware/errorHandler');
const logger = require('./middleware/loggerMiddleware');

const usersRoutes = require('./routes/userRoutes');
const notesRoutes = require('./routes/notesRoutes');
const authRoutes = require('./routes/authRoutes');

const connectDB = require('./config/db');
const port = process.env.PORT || 8080;

connectDB();
const app = express();

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.use(cors());

app.use(logger);

app.use('/api/users', usersRoutes);
app.use('/api/notes', notesRoutes);
app.use('/api/auth', authRoutes);

app.use(errorHandler);

app.listen(port, () => {
  console.log(`Server runs on port: ${port}`.blue.underline);
});
