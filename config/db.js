const mongoose = require('mongoose');

const connectDB = async () => {
  try {
    // eslint-disable-next-line no-undef
    const connToDB = await mongoose.connect(process.env.MONGO_URI);

    console.log(
        `MongoDB connected ${connToDB.connection.host}`.yellow.underline,
    );
  } catch (error) {
    console.log(error);
    // eslint-disable-next-line no-undef
    process.exit(1);
  }
};

module.exports = connectDB;
