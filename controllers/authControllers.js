const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const asyncHandler = require('express-async-handler');
const User = require('../models/userModel');

// @desc Register a new user
// @route POST /api/auth/register
// @access Public
const createUser = asyncHandler(async (req, res) => {
  const {username, password} = req.body;

  if (!username || !password) {
    res.status(400);
    throw new Error('Please add all fields');
  }

  // check if user exists

  const userExists = await User.findOne({username});

  if (userExists) {
    res.status(400);
    throw new Error('Username already exists');
  }

  // hash password

  const salt = await bcrypt.genSalt(8);
  const hashedPassword = await bcrypt.hash(password, salt);

  // create user

  await User.create({
    username,
    password: hashedPassword,
    createdDate: new Date().toISOString(),
  });
  res.status(200).json({
    message: 'Success',
  });
});

// @desc Login
// @route POST /api/auth/login
// @access Public
const loginUser = asyncHandler(async (req, res) => {
  const {username, password} = req.body;

  // check for username
  const user = await User.findOne({username});

  if (user && (await bcrypt.compare(password, user.password))) {
    res.status(200).json({
      message: 'Success',
      jwt_token: generateToken(user.id),
    });
  } else {
    res.status(400);
    throw new Error('Invalid credentials');
  }
});

// generate JWT
const generateToken = (id) => {
  // eslint-disable-next-line no-undef
  return jwt.sign({id}, process.env.JWT_SECRET, {expiresIn: '30d'});
};

module.exports = {
  createUser,
  loginUser,
};
