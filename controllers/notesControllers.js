const asyncHandler = require('express-async-handler');
const User = require('../models/userModel');
const Note = require('../models/noteModel');

// @desc Get user's notes
// @route GET /api/notes/
// @access Private
const getNotes = asyncHandler(async (req, res) => {
  const userId = req.user.id;
  const offset = +req.query.offset;
  const limit = +req.query.limit;
  const notes = await Note.find({userId}).skip(offset).limit(limit);

  res.status(200).json({
    offset,
    limit,
    count: notes.length,
    notes,
  });
});

// @desc Add Note for User
// @route POST /api/notes/
// @access Private
const createNotes = asyncHandler(async (req, res) => {
  if (!req.body.text) {
    res.status(400);
    throw new Error('Please add a text');
  }

  await Note.create({
    userId: req.user.id,
    completed: false,
    text: req.body.text,
    createdDate: new Date().toISOString(),
  });

  res.status(200).json({
    message: 'Success',
  });
});

// @desc Get user's note by id
// @route GET /api/notes/id
// @access Private
const getNoteById = asyncHandler(async (req, res) => {
  const note = await Note.findById(req.params.id);

  if (!note) {
    res.status(400);
    throw new Error('Note not found');
  }

  const user = await User.findById(req.user.id);

  // check for user
  if (!user) {
    res.status(400);
    throw new Error('User not found');
  }

  if (note.userId.toString() !== user.id) {
    res.status(400);
    throw new Error('User not authorized');
  }

  res.status(200).json({
    note: {
      _id: note._id,
      userId: note.userId,
      completed: note.completed,
      text: note.text,
      createdDate: note.createdDate,
    },
  });
});

// @desc Update user's note by id
// @route PUT /api/notes/id
// @access Private
const updateNote = asyncHandler(async (req, res) => {
  const note = await Note.findById(req.params.id);

  if (!note) {
    res.status(400);
    throw new Error('Note not found');
  }

  const user = await User.findById(req.user.id);

  // check for user
  if (!user) {
    res.status(400);
    throw new Error('User not found');
  }

  if (note.userId.toString() !== user.id) {
    res.status(400);
    throw new Error('User not authorized');
  }

  await Note.findByIdAndUpdate(
      req.params.id,
      {text: req.body.text},
      {new: true},
  );

  res.status(200).json({
    message: 'Success',
  });
});

// @desc Check/uncheck user's note by id
// @route PATCH /api/notes/id
// @access Private
const checkNote = asyncHandler(async (req, res) => {
  const note = await Note.findById(req.params.id);

  if (!note) {
    res.status(400);
    throw new Error('Note not found');
  }

  const user = await User.findById(req.user.id);

  // check for user
  if (!user) {
    res.status(400);
    throw new Error('User not found');
  }

  if (note.userId.toString() !== user.id) {
    res.status(400);
    throw new Error('User not authorized');
  }

  await Note.findByIdAndUpdate(
      req.params.id,
      {completed: !note.completed},
      {new: true},
  );

  res.status(200).json({
    message: 'Success',
  });
});

// @desc Delete user's note by id
// @route DELETE /api/notes/id
// @access Private
const deleteNote = asyncHandler(async (req, res) => {
  const note = await Note.findById(req.params.id);

  if (!note) {
    res.status(400);
    throw new Error('Note not found');
  }

  const user = await User.findById(req.user.id);

  // check for user
  if (!user) {
    res.status(400);
    throw new Error('User not found');
  }

  if (note.userId.toString() !== user.id) {
    res.status(400);
    throw new Error('User not authorized');
  }

  await Note.deleteOne();

  res.status(200).json({
    message: 'Success',
  });
});

module.exports = {
  getNotes,
  createNotes,
  getNoteById,
  updateNote,
  checkNote,
  deleteNote,
};
