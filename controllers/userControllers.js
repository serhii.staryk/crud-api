const asyncHandler = require('express-async-handler');
const bcrypt = require('bcryptjs');
const User = require('../models/userModel');

// @desc Get user's profile info
// @route GET /api/users/me
// @access Private
const getProfile = asyncHandler(async (req, res) => {
  const {_id, username, createdDate} = await User.findById(req.user.id);
  res.status(200).json({
    user: {
      _id,
      username,
      createdDate,
    },
  });
});

// @desc Delete user's profile
// @route DELETE /api/users/me
// @access Private
const deleteProfile = asyncHandler(async (req, res) => {
  const user = await User.findById(req.user.id);

  if (!user) {
    res.status(400);
    throw new Error('User not authorized');
  }

  await User.deleteOne();

  res.status(200).json({
    message: 'Success',
  });
});

// @desc Change user's password
// @route PATCH /api/users/me
// @access Private
const changePassword = asyncHandler(async (req, res) => {
  const oldPassword = req.body.oldPassword;
  const newPassword = req.body.newPassword;
  const user = await User.findById(req.user.id);

  if (await bcrypt.compare(oldPassword, user.password)) {
    // hash password

    const salt = await bcrypt.genSalt(8);
    const hashedPassword = await bcrypt.hash(newPassword, salt);

    await User.findByIdAndUpdate(
        user.id,
        {password: hashedPassword},
        {
          new: true,
        },
    );
    res.status(200).json({
      message: 'Success',
    });
  }
});

module.exports = {
  getProfile,
  deleteProfile,
  changePassword,
};
