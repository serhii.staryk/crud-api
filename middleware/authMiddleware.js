const jwt = require('jsonwebtoken');
const asyncHandler = require('express-async-handler');
const User = require('../models/userModel');

const protectRoute = asyncHandler(async (req, res, next) => {
  let jwttoken;
  if (
    (req.headers.authorization &&
      req.headers.authorization.startsWith('Bearer')) ||
    (req.headers.authorization && req.headers.authorization.startsWith('JWT'))
  ) {
    try {
      // get token from header

      jwttoken = req.headers.authorization.split(' ')[1];

      // Verify token

      // eslint-disable-next-line no-undef
      const decoded = jwt.verify(jwttoken, process.env.JWT_SECRET);

      // Get user from token

      req.user = await User.findById(decoded.id).select('-password');
      next();
    } catch (error) {
      console.error(error);
      res.status(400);
      throw new Error('Not authorized');
    }
  }

  if (!jwttoken) {
    res.status(400);
    throw new Error('Not authorized, no token');
  }
});

module.exports = protectRoute;
