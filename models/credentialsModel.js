/* eslint-disable new-cap */
const mongoose = require('mongoose');

const credentialsScheme = mongoose.Schema({
  username: {
    type: String,
    required: [true, 'Please add a username'],
  },
  password: {
    type: String,
    required: [true, 'Please add a password'],
  },
});

module.exports = mongoose.model('Credentials', credentialsScheme);
