const mongoose = require('mongoose');

// eslint-disable-next-line new-cap
const noteSchema = mongoose.Schema({
  userId: {
    type: String,
  },
  completed: {
    type: Boolean,
  },
  text: {
    type: String,
  },
  createdDate: {
    type: String,
  },
});

module.exports = mongoose.model('Note', noteSchema);
