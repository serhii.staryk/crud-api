const mongoose = require('mongoose');

// eslint-disable-next-line new-cap
const userSchema = mongoose.Schema({
  username: {
    type: String,
    unique: true,
  },
  password: {
    type: String,
  },
  createdDate: {
    type: String,
  },
});

module.exports = mongoose.model('User', userSchema);
