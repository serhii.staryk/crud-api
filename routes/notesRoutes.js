/* eslint-disable new-cap */
const express = require('express');
const {
  getNotes,
  createNotes,
  getNoteById,
  updateNote,
  checkNote,
  deleteNote,
} = require('../controllers/notesControllers');

const protectRoute = require('../middleware/authMiddleware');

const router = express.Router();

router.get('/', protectRoute, getNotes);
router.post('/', protectRoute, createNotes);

router.get('/:id', protectRoute, getNoteById);
router.put('/:id', protectRoute, updateNote);
router.patch('/:id', protectRoute, checkNote);
router.delete('/:id', protectRoute, deleteNote);

module.exports = router;
