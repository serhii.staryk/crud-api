/* eslint-disable new-cap */
const express = require('express');

const {
  getProfile,
  deleteProfile,
  changePassword,
} = require('../controllers/userControllers');

const router = express.Router();

const protectRoute = require('../middleware/authMiddleware');

router.get('/me', protectRoute, getProfile);
router.delete('/me', protectRoute, deleteProfile);
router.patch('/me', protectRoute, changePassword);

module.exports = router;
